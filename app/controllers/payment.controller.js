var Payment = require('mongoose').model('Payment');
var userTest = require('mongoose').model('UserTest');
var TestResult = require('mongoose').model('TestResult');
var testFile = require('mongoose').model('testFile');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');

exports.listPayment = function (req, res, next) {
    logger.info("Listing all Payment");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = -1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType) { 
        if(req.query.sortType == "transactionId")
            req.query.sortType = "_id"
        sort = {}; 
        sort[req.query.sortType] = 1;
     }
    if (!!req.query.sortBy) {
        if (req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if (req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if (!!req.query.search) {
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function (element) {
            if(element.split(':')[0] == "transactionId")
                element.split(':')[0] = "_id"

            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in Payment.schema.paths) {
            if (Payment.schema.paths.hasOwnProperty(property) && Payment.schema.paths[property]["instance"] === "String" && property != "fileSlip") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    Payment.find(query, { fileSlip: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, cc) {
        if (err) {
            logger.error("There was an error while trying to list all Payment [" + err.code + ": " + err.errmsg + "]");
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.info("All Payment listed successfully");
            var returnData = {};
            var dataTemp = JSON.stringify(cc);
            cc = JSON.parse(dataTemp);
            returnData.data = cc;
            returnData.data.forEach(function(e) {
                //e.create = new Date( e.create );
                e.transactionId = e._id;
                delete e._id;
                // console.log(timeTemp.getDate());
                // e.create = timeTemp.getDate()+"/"+(timeTemp.getMonth())+"/"+timeTemp.getFullYear()+", "+timeTemp.getHours()+":"+timeTemp.getMinutes()+":"+timeTemp.getSeconds()
            }, this);


            Payment.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if (!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.paymentDetail = function (req, res, next) {
    logger.debug(req.body.transactionId + "call paymentDetail");
    //console.log("req.body.transactionId: ", req.body.transactionId)
    Payment.findOne({
        _id: req.body.transactionId
    }, { create: 0, __v: 0 }, function (err, pay) {
        if (err) {
            logger.debug(req.body.transactionId + "paymentDetail Error");
            res.json({
                errorCode: error.code_01003,
                errorDesc: error.desc_01003,
            });
        } else {
            //console.log("pay: ", pay)
            pay.transactionId = pay._id;
            delete pay._id;

            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
                data: pay
            });
        }
    });
};
// wait_upload
// wait_approve
// approved
// reject

exports.paymentReject = function (req, res, next) {
    logger.debug(req.body.transactionId + "call paymentReject");
    //status = "reject";
    //console.log("req.body.transactionId: ", req.body.transactionId)
    Payment.findOneAndUpdate({
        _id: req.body.transactionId
    },{ status: "reject" }, { create: 0, __v: 0 }, function (err, pay) {
        if (err) {
            logger.debug(req.body.transactionId + "paymentReject Error");
            res.json({
                errorCode: error.code_01003,
                errorDesc: error.desc_01003,
            });
        } else {
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000
            });
        }
    });
};

exports.paymentApprove = function (req, res, next) {
    logger.debug(req.body.transactionId + "call paymentApprove");
    //status = "reject";
    //console.log("req.body.transactionId: ", req.body.transactionId)
    Payment.findOne({
        _id: req.body.transactionId
    }, { create: 0, __v: 0 }, function (err, pay) {
        if (err) {
            logger.debug(req.body.transactionId + "paymentApprove Error");
            res.json({
                errorCode: error.code_01003,
                errorDesc: error.desc_01003,
            });
        } else {
            if (pay.status == "wait_approve") {
                var listGroup = [];
                var listSubject = [];
                var d1 = 0;

                testFile.find({}, { create: 0, __v: 0, filePath: 0 }, function (err, test) {

                    for (let i = 0; i < test.length; i++) {
                        for (let j = 0; j < pay.order.length; j++) {
                            var skip = false;
                            for (let k = 0; k < pay.order[j].data.length; k++) {
                                if(test[i].testName == pay.order[j].data[k].testName){
                                    pay.order[j].data[k].testId = test[i].testId
                                    skip = true;
                                    break
                                }
                            }
                            if(skip)
                                break;
                            
                        }
                    }

                    console.log("Testtttttt:", pay.order[0].data)

                    pay.order.forEach(function (e) {
                        listGroup.push(e.groupName);
                        listSubject[d1] = [];
                        e.data.forEach(function (inner_e) {
                            var dataPush = {}
                            dataPush.quantity = inner_e.quantity;
                            dataPush.testName = inner_e.testId
                            listSubject[d1].push(dataPush);
                        }, this);
                        d1++;
                    }, this);

                    userTest.findOne({
                        username: pay.username
                    }, { _id: 0, create: 0, __v: 0 }, function (err, UserTest) {
                        if (err) {
                            res.json({
                                errorCode: err.code,
                                errorDesc: err.errmsg,
                            });
                        } else {
                            var data = {};
                            data.username = UserTest.username;
                            data.test = [];

                            var Subject = {};
                            for (var i = 0; i < UserTest.test.length; i++) {
                                var Subject = {};
                                Subject.name = UserTest.test[i].name;
                                Subject.Used = UserTest.test[i].Used;
                                Subject.Inused = UserTest.test[i].Inused;
                                Subject.remain = UserTest.test[i].remain;
                                hasData = true;

                                data.test.push(Subject);
                            }
                            d1 = 0;
                            listGroup.forEach(function (group) {
                                listSubject[d1].forEach(function (e) {
                                    var Subject = {};
                                    var hasData = false;
                                    for (var i = 0; i < data.test.length; i++) {
                                        if (data.test[i].name == e.testName) {
                                            data.test[i].remain = data.test[i].remain + e.quantity;
                                            hasData = true;
                                            break;
                                        }
                                    }
                                    if (!hasData) {
                                        Subject.name = e.testName;
                                        Subject.Used = 0;
                                        Subject.Inused = 0;
                                        Subject.remain = e.quantity;

                                        data.test.push(Subject);
                                    }

                                }, this);
                                d1++;
                            }, this);

                            userTest.findOneAndUpdate({ username: pay.username }, data, function (err, userTest) {
                                if (err) {
                                    logger.error("Can't add test count", pay.username);
                                    res.json({
                                        errorCode: err.code,
                                        errorDesc: err.errmsg,
                                    });
                                } else {
                                    logger.info("Add test count successfully", pay.username);
                                    TestResult.findOne({
                                        username: pay.username
                                    }, { _id: 0, create: 0, __v: 0 }, function (err, TestR) {
                                        if (err) {
                                            res.json({
                                                errorCode: err.code,
                                                errorDesc: err.errmsg,
                                            });
                                        } else {
                                            var dataTemp = JSON.stringify(TestR);
                                            var dataResult = JSON.parse(dataTemp);
                                            var noResult = false;
                                            if (dataResult == null || typeof dataResult == 'undefined') {
                                                dataResult = {};
                                                dataResult.username = pay.username;
                                                noResult = true;
                                            }

                                            if (dataResult.results == null || typeof dataResult.results == 'undefined')
                                                dataResult.results = [];

                                            d1 = 0;
                                            listGroup.forEach(function (group) {
                                                var notEmpty = true;
                                                while (notEmpty) {
                                                    var Subject = {};
                                                    notEmpty = false;

                                                    listSubject[d1].forEach(function (e) {
                                                        if (e.quantity > 0) {
                                                            Subject[e.testName] = -1;
                                                            e.quantity--;
                                                            notEmpty = true;
                                                        }
                                                    }, this);
                                                    if (Object.keys(Subject).length > 0)
                                                        dataResult.results.push(Subject);
                                                }
                                                d1++;
                                            }, this);
                                            if (!noResult) {
                                                TestResult.findOneAndUpdate({ username: pay.username }, dataResult, function (err, userTest) {
                                                    if (err) {
                                                        logger.error("Can't add pre-testResult successfully", pay.username);
                                                        res.json({
                                                            errorCode: err.code,
                                                            errorDesc: err.errmsg,
                                                        });
                                                    } else {
                                                        logger.info("User Edit successfully", pay.username);

                                                        Payment.findOneAndUpdate({
                                                            _id: req.body.transactionId
                                                        }, { status: "approved" }, { create: 0, __v: 0 }, function (err, pay) {
                                                            if (err) {
                                                                logger.debug(req.body.transactionId + "paymentApprove Error");
                                                                res.json({
                                                                    errorCode: error.code_01003,
                                                                    errorDesc: error.desc_01003,
                                                                });
                                                            } else {
                                                                res.json({
                                                                    errorCode: error.code_00000,
                                                                    errorDesc: error.desc_00000
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                            else {
                                                var test = new TestResult(dataResult);
                                                test.save(function (err) {
                                                    if (err) {
                                                        console.log("err: ", err);
                                                        var errMessage = err.errmsg;
                                                        if (err.code == "11000") errMessage = "Username already exist."
                                                        logger.error("There was an error while trying to register new user [" + err.code + ": " + err.errmsg + "]", user.username);
                                                        res.json({
                                                            errorCode: err.code,
                                                            errorDesc: errMessage,
                                                        });
                                                    } else {
                                                        Payment.findOneAndUpdate({
                                                            _id: req.body.transactionId
                                                        }, { status: "approved" }, { create: 0, __v: 0 }, function (err, pay) {
                                                            if (err) {
                                                                logger.debug(req.body.transactionId + "paymentApprove Error");
                                                                res.json({
                                                                    errorCode: error.code_01003,
                                                                    errorDesc: error.desc_01003,
                                                                });
                                                            } else {
                                                                res.json({
                                                                    errorCode: error.code_00000,
                                                                    errorDesc: error.desc_00000
                                                                });
                                                            }
                                                        });
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            });
                        }
                    })
                })
            }
            else {
                res.json({
                    errorCode: error.code_00012,
                    errorDesc: error.desc_00012
                });
            }
        }
    });
};