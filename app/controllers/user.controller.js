var User = require('mongoose').model('User');
var testFile = require('mongoose').model('testFile');
var schoolDB = require('mongoose').model('school');
var userTest = require('mongoose').model('UserTest');
var TestResult = require('mongoose').model('TestResult');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');
var crypto = require('crypto');
var utility = require('../../config/utility');
var os = require('os');
var ifaces = os.networkInterfaces();
var uid = require('rand-token').uid;
var algorithm = 'aes-256-ctr';

exports.create = function (req, res, next) {
    var user = new User(req.body);
    logger.info("Creating new user", user.username)
    if (req.body.email != "") {
        req.checkBody('email', 'Invalid email').isEmail();
        req.sanitizeBody('email').normalizeEmail();
        var errors = req.validationErrors();
        if (errors) {
            logger.error("The entered email is in incorrect format", user.username);
            res.json({
                errorCode: error.code_00010,
                errorDesc: error.desc_00010,
            });
            return;
        }
    }

    user.save(function (err) {
            if (err) {
                console.log("err: ", err);
                var errMessage = err.errmsg;
                if (err.code == "11000") errMessage = "Username already exist."
                logger.error("There was an error while trying to register new user [" + err.code + ": " + err.errmsg + "]", user.username);
                res.json({
                    errorCode: err.code,
                    errorDesc: errMessage,
                });
            } else {
                logger.info("Create user successfully", user.username);
                var data = {};
                var listSubject = [];

                testFile.find({}, { testId:1, isBrand: 1 }, function (err, tests) {
                    
                    tests.forEach(function(e) {
                        if(!e.isBrand){
                            listSubject.push(e.testId);
                        }
                    }, this);
                    // if (req.body.examGroup == "A")
                    //     listSubject = ["phyA", "chemA", "bioA", "engA", "thaiA", "socialA", "mathA1", "mathA2", "sciA"]
                    // else if (req.body.examGroup == "B")
                    //     listSubject = ["ONET_mathB", "ONET_thaiB", "ONET_socialB", "ONET_sciB", "ONET_engB", "GAT_1B", "GAT_2B", "PAT_1B", "PAT_2B", "PAT_3B", "PAT_5B"]
                    //testId
                    data.username = user.username;
                    data.test = []
                    listSubject.forEach(function (e) {
                        var Subject = {};
                        Subject.name = e;
                        Subject.Used = 0;
                        Subject.Inused = 0;
                        // if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined')
                        //     Subject.remain = 999;
                        // else
                            Subject.remain = 0;

                        data.test.push(Subject);
                    }, this);

                    var userT = new userTest(data);
                    userT.save(function (err) {
                        if (err) {
                            logger.error("There was an error while trying to populate available subjects for user [" + error.code_00005 + ": " + error.desc_00005 + "]", user.username);
                        } else {
                            logger.info("Available subjects populated successfully", user.username);

                            var dataResult = {};
                            dataResult.username = user.username;
                            dataResult.results = [];
                            // if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined') {
                            //     var Subject = {};
                            //     listSubject.forEach(function (e) {
                            //         Subject[e] = -1;
                            //     }, this);
                            //     dataResult.results.push(Subject);
                            // }
                            var testResult = new TestResult(dataResult);

                            testResult.save(function (err) {
                                if (err) {
                                    logger.error("There was an error while trying to populate available answer sheets for user [" + error.code_00005 + ": " + error.desc_00005 + "]", user.username);
                                } else {
                                    logger.info("Available answer sheets populated successfully", user.username);

                                    res.json({
                                        errorCode: error.code_00000,
                                        errorDesc: error.desc_00000,
                                    });
                                }
                            });
                        }
                    });
                })
            }
        })
};

exports.register = function (req, res, next) {
    var user = new User(req.body);
    logger.info("Registering new user", user.username);

    user.isActive = true;
    user.studyClass = "";
    user.schoolProvince = user.studyClass.trim();
    user.schoolCode = user.schoolCode.trim();

    if (req.body.email != "") {
        req.checkBody('email', 'Invalid email').isEmail();
        req.sanitizeBody('email').normalizeEmail();
        var errors = req.validationErrors();
        if (errors) {
            logger.error("The entered email is in incorrect format", user.username);
            res.json({
                errorCode: error.code_00010,
                errorDesc: error.desc_00010,
            });
            return;
        }
    }

    if (req.body.examGroup == null || typeof req.body.examGroup == 'undefined') {
        logger.error("Exam group is missing from received request", user.username);
        res.json({
            errorCode: error.code_00005,
            errorDesc: error.desc_00005 + " : examGroup",
        });
        return;
    }

    new Promise((resolve, reject) => {
        var updateData = {};
        schoolDB.findOne({ SCHOOL_ID: user.schoolCode }, function (err, userSchool) {
            if (!!userSchool) {
                //console.log("userSchool: ", userSchool[0])
                var userTemp = JSON.stringify(userSchool);
                userSchool = JSON.parse(userTemp);
                updateData = { schoolName: userSchool.SCHOOL_NAME, schoolProvince: userSchool.PROVINCE_NAME, schoolDistrict: userSchool.DISTRICT_NAME, schoolRegion: userSchool.SCHOOL_REGION };

                console.log("updateData: ", updateData)
                resolve(updateData);
            }
            else
                resolve(updateData);
        })
    }).then(function (data) {

        console.log("data: ", data)

        if(user.schoolProvince == null || typeof user.schoolProvince == "undefined" || user.schoolProvince == "" ){
            user.schoolProvince = data.schoolProvince;
        }
        if(user.schoolDistrict == null || typeof user.schoolDistrict == "undefined" || user.schoolDistrict == "" ){
            user.schoolDistrict = data.schoolDistrict;
        }
        user.schoolRegion = data.schoolRegion;
        user.schoolName = data.schoolName;


        user.save(function (err) {
            if (err) {
                console.log("err: ", err);
                var errMessage = err.errmsg;
                if (err.code == "11000") errMessage = "Username already exist."
                logger.error("There was an error while trying to register new user [" + err.code + ": " + err.errmsg + "]", user.username);
                res.json({
                    errorCode: err.code,
                    errorDesc: errMessage,
                });
            } else {
                logger.info("Create user successfully", user.username);
                var data = {};
                var listSubject = [];
                if (req.body.examGroup == "A")
                    listSubject = ["phyA", "chemA", "bioA", "engA", "thaiA", "socialA", "mathA1", "mathA2", "sciA"]
                else if (req.body.examGroup == "B")
                    listSubject = ["ONET_mathB", "ONET_thaiB", "ONET_socialB", "ONET_sciB", "ONET_engB", "GAT_1B", "GAT_2B", "PAT_1B", "PAT_2B", "PAT_3B", "PAT_5B"]
                data.username = user.username;
                data.test = []
                listSubject.forEach(function (e) {
                    var Subject = {};
                    Subject.name = e;
                    Subject.Used = 0;
                    Subject.Inused = 0;
                    if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined')
                        Subject.remain = 1;
                    else
                        Subject.remain = 0;

                    data.test.push(Subject);
                }, this);

                var userT = new userTest(data);
                userT.save(function (err) {
                    if (err) {
                        logger.error("There was an error while trying to populate available subjects for user [" + error.code_00005 + ": " + error.desc_00005 + "]", user.username);
                    } else {
                        logger.info("Available subjects populated successfully", user.username);

                        var dataResult = {};
                        dataResult.username = user.username;
                        dataResult.results = [];
                        if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined') {
                            var Subject = {};
                            listSubject.forEach(function (e) {
                                Subject[e] = -1;
                            }, this);
                            dataResult.results.push(Subject);
                        }
                        var testResult = new TestResult(dataResult);

                        testResult.save(function (err) {
                            if (err) {
                                logger.error("There was an error while trying to populate available answer sheets for user [" + error.code_00005 + ": " + error.desc_00005 + "]", user.username);
                            } else {
                                logger.info("Available answer sheets populated successfully", user.username);

                                res.json({
                                    errorCode: error.code_00000,
                                    errorDesc: error.desc_00000,
                                });
                            }
                        });
                    }
                });

            }
        })
    });
};

exports.list = function (req, res, next) {
    logger.info("Listing all users");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['firstName'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType) { sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if (req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if (req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if (!!req.query.search) {
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function (element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in User.schema.paths) {
            if (User.schema.paths.hasOwnProperty(property) && User.schema.paths[property]["instance"] === "String" && property != "salt" && property != "password") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    User.find(query, { _id: 0, salt: 0, password: 0, create: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, users) {
        if (err) {
            logger.error("There was an error while trying to list all users [" + err.code + ": " + err.errmsg + "]");
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.info("All users listed successfully");
            var returnData = {};
            returnData.data = users;

            User.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if (!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.update = function (req, res, next) {
    if (!!req.user) {
        var isOldPassCorrect = true;
        if (!!req.body.password) {
            if (!!req.body.oldPassword) {
                var hashpwd = crypto.pbkdf2Sync(req.body.oldPassword, req.user.salt, 100, 64,'sha1').toString('base64');

                if (hashpwd == req.user.password) {
                    req.body.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
                    req.body.password = crypto.pbkdf2Sync(req.body.password, req.body.salt.toString(), 100, 64,'sha1').toString('base64');
                }
                else
                    isOldPassCorrect = false;
            }
            else {
                //bypass first time login
                req.body.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
                req.body.password = crypto.pbkdf2Sync(req.body.password, req.body.salt.toString(), 100, 64,'sha1').toString('base64');
            }
        }

        if (!!req.body.isFirstLogin && req.body.isFirstLogin) {
            req.body.isActive = false;
            req.body.isFirstTime = false;
        }
        if (isOldPassCorrect) {
            User.findOneAndUpdate({ username: req.user.username }, req.body, function (err, user) {
                if (err) {
                    //return next(err);
                    res.json({
                        errorCode: err.code,
                        errorDesc: err.errmsg,
                    });
                } else {
                    res.json({
                        errorCode: error.code_00000,
                        errorDesc: error.desc_00000,
                    });
                }
            });
        }
        else {
            res.json({
                errorCode: error.code_00011,
                errorDesc: error.desc_00011,
            });
        }
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.updateReg = function (req, res, next) {
    if (!!req.user) {
        var isOldPassCorrect = true;
        if (!!req.body.password) {
            if (!!req.body.oldPassword) {
                var hashpwd = crypto.pbkdf2Sync(req.body.oldPassword, req.user.salt, 100, 64, 'sha1').toString('base64');

                if (hashpwd == req.user.password) {
                    req.body.salt = crypto.randomBytes(16).toString('base64');
                    req.body.password = crypto.pbkdf2Sync(req.body.password, req.body.salt.toString(), 100, 64, 'sha1').toString('base64');
                }
                else
                    isOldPassCorrect = false;
            }
            else {
                //bypass first time login
                req.body.salt = crypto.randomBytes(16).toString('base64');
                req.body.password = crypto.pbkdf2Sync(req.body.password, req.body.salt.toString(), 100, 64, 'sha1').toString('base64');
            }
        }

        if (!!req.body.isFirstLogin && req.body.isFirstLogin) {
            req.body.isActive = false;
            req.body.isFirstTime = false;
        }

        if (isOldPassCorrect) {
            logger.info("Update User Information", req.user.username);

            User.findOne({ username: req.user.username }, { _id: 0, create: 0, __v: 0 }, function (err, tempUser) {

                if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined') {
                    tempUser.brandCode.push(req.body.brandCode);
                    req.body.brandCode = tempUser.brandCode;
                }

                User.findOneAndUpdate({ username: req.user.username }, req.body, function (err, user) {
                    if (err) {
                        //return next(err);
                        logger.error("Can't update user inforamtion", req.user.username);
                        res.json({
                            errorCode: err.code,
                            errorDesc: err.errmsg,
                        });
                    } else {
                        userTest.findOne({
                            username: req.user.username
                        }, { _id: 0, create: 0, __v: 0 }, function (err, UserTest) {
                            if (err) {
                                res.json({
                                    errorCode: err.code,
                                    errorDesc: err.errmsg,
                                });
                            } else {
                                
                                var data = {};
                                data.username = UserTest.username;
                                data.test = [];
                                var listSubject = [];
                                if (req.body.examGroup == "A")
                                    listSubject = ["phyA", "chemA", "bioA", "engA", "thaiA", "socialA", "mathA1", "mathA2", "sciA"]
                                else if (req.body.examGroup == "B")
                                    listSubject = ["ONET_mathB", "ONET_thaiB", "ONET_socialB", "ONET_sciB", "ONET_engB", "GAT_1B", "GAT_2B", "PAT_1B", "PAT_2B", "PAT_3B", "PAT_5B"]

                                var Subject = {};
                                for (var i = 0; i < UserTest.test.length; i++) {
                                    var Subject = {};
                                    Subject.name = UserTest.test[i].name;
                                    Subject.Used = UserTest.test[i].Used;
                                    Subject.Inused = UserTest.test[i].Inused;
                                    Subject.remain = UserTest.test[i].remain;
                                    hasData = true;

                                    data.test.push(Subject);
                                }

                                listSubject.forEach(function (e) {
                                    var Subject = {};
                                    var hasData = false;
                                    for (var i = 0; i < data.test.length; i++) {
                                        if (data.test[i].name == e) {
                                            if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined')
                                                data.test[i].remain = data.test[i].remain + 1;
                                            else
                                                data.test[i].remain = data.test[i].remain;
                                            hasData = true;
                                            break;
                                        }
                                    }
                                    if (!hasData) {
                                        Subject.name = e;
                                        Subject.Used = 0;
                                        Subject.Inused = 0;
                                        if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined')
                                            Subject.remain = 1;
                                        else
                                            Subject.remain = 0;

                                        data.test.push(Subject);
                                    }
                                    
                                }, this);

                                userTest.findOneAndUpdate({ username: req.user.username }, data, function (err, userTest) {
                                    if (err) {
                                        logger.error("Can't add test count", req.user.username);
                                        res.json({
                                            errorCode: err.code,
                                            errorDesc: err.errmsg,
                                        });
                                    } else {
                                        logger.info("Add test count successfully", req.user.username);
                                        TestResult.findOne({
                                            username: req.user.username
                                        }, { _id: 0, create: 0, __v: 0 }, function (err, TestR) {
                                            if (err) {
                                                res.json({
                                                    errorCode: err.code,
                                                    errorDesc: err.errmsg,
                                                });
                                            } else {
                                                var dataTemp = JSON.stringify(TestR);
                                                var dataResult = JSON.parse(dataTemp);
                                                console.log("dataResult: ", dataResult);
                                                if (req.body.brandCode !== null && typeof req.body.brandCode !== 'undefined') {
                                                    var Subject = {};
                                                    if (req.body.examGroup == "A")
                                                        listSubject = ["phyA", "chemA", "bioA", "engA", "thaiA", "socialA", "mathA1", "mathA2", "sciA"]
                                                    else if (req.body.examGroup == "B")
                                                        listSubject = ["ONET_mathB", "ONET_thaiB", "ONET_socialB", "ONET_sciB", "ONET_engB", "GAT_1B", "GAT_2B", "PAT_1B", "PAT_2B", "PAT_3B", "PAT_5B"]

                                                    listSubject.forEach(function (e) {
                                                        Subject[e] = -1;
                                                    }, this);
                                                    if(dataResult.results == null || typeof dataResult.results == 'undefined')
                                                        dataResult.results = [];

                                                    dataResult.results.push(Subject);
                                                }

                                                TestResult.findOneAndUpdate({ username: req.user.username }, dataResult, function (err, userTest) {
                                                    if (err) {
                                                        logger.error("Can't add pre-testResult successfully", req.user.username);
                                                        res.json({
                                                            errorCode: err.code,
                                                            errorDesc: err.errmsg,
                                                        });
                                                    } else {
                                                        logger.info("User Edit successfully", req.user.username);

                                                        res.json({
                                                            errorCode: error.code_00000,
                                                            errorDesc: error.desc_00000,
                                                        });
                                                    }
                                                });
                                            }
                                        })


                                    }
                                });


                            }
                        });
                    }
                });
            });
        }
        else {
            res.json({
                errorCode: error.code_00011,
                errorDesc: error.desc_00011,
            });
        }
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.read = function (req, res) {
    if (!!req.user) {
        res.json(req.user);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.delete = function (req, res, next) {
    if (!!req.user) {
        req.user.remove(function (err) {
            if (err) {
                return next(err);
            } else {
                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.userByUsername = function (req, res, next, username) {
    User.findOne({
        username: username
    }, { create: 0, __v: 0 }, function (err, user) {
        if (err) {
            return next(err);
        } else {
            req.user = user;
            next();
        }
    });
};

exports.userByUsernameBrand = function (req, res, next, username) {
    username = utility.decryptSecret(username,req.session.hashtoken);
    User.findOne({
        username: username
    }, { _id: 0, create: 0, __v: 0 }, function (err, user) {
        if (err) {
            return next(err);
        } else {
            req.user = user;
            next();
        }
    });
};

exports.checkUsername = function (req, res, next) {
    User.findOne({
        username: req.body.username
    }, function (err, user) {
        if (err) {
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            if (!!user)
                res.json(true);
            else
                res.json(false);
        }
    });
};

exports.checkEmail = function (req, res, next) {
    User.findOne({
        email: req.body.email
    }, function (err, email) {
        if (err) {
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            if (!!email)
                res.json(true);
            else
                res.json(false);
        }
    });
};

exports.checkIdCard = function (req, res, next) {
    User.find({
        idCard: req.body.idCard,
    }, function (err, data) {
        if (err) {
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            console.log("data: ", data);
            if (!!data && data.length > 0) {
                console.log("data: ", data);
                console.log("data.username: ", data[0].username);
                if (data[0].username == req.body.idCard)
                    res.json({ used: false, isUpdate: true });
                else
                    res.json({ used: true, isUpdate: false });
            }
            else
                res.json({ used: false, isUpdate: false });
        }
    });
};



exports.login = function (req, res) {
    logger.info("Logging in...", req.body.username);
    req.checkBody({
        username: {
            notEmpty: true
        },
        password: {
            notEmpty: true
        }
    });
    logger.info("Validating entered credential", req.body.username);
    var errors = req.validationErrors();
    if (errors) {
        logger.error("User validation failed", req.body.username);
        res.json({
            errorCode: error.code_00001,
            errorDesc: error.desc_00001,
            isLoggedIn: false
        });
        return;
    }
    var query = User.where({ $or: [{ username: req.body.username }, { idCard: req.body.username }] });
    query.findOne(function (err, data) {
        if (err) {
            logger.error("There was an error while trying to login [" + err.code + ": " + err.errmsg + "]", req.body.username);
            res.json({
                errorCode: error.code_00001,
                errorDesc: error.desc_00001,
                isLoggedIn: false
            });
        }
        if (!!data && data.isActive) {
            var hashpwd = crypto.pbkdf2Sync(req.body.password, data.salt, 100, 64,'sha1').toString('base64');
            var password2 = req.body.password.toString().replace(/^0+(?=.)/g, '')
            var hashpwd2 = crypto.pbkdf2Sync(password2, data.salt, 100, 64,'sha1').toString('base64');
            var salt2 = new Buffer(data.salt, 'base64');
            var hashpwd3 = crypto.pbkdf2Sync(req.body.password, salt2, 100, 64,'sha1').toString('base64');

            if (hashpwd == data.password || hashpwd2 == data.password || hashpwd3 == data.password) {
                logger.info("Login successfully", req.body.username);

                req.session.username = req.body.username;
                req.session.hashtoken = uid(16);
                req.session.hashtokenforSubject = uid(16);
                
                if (req.body.remember === 'remember') {
                    req.session.remember = true;
                }

                var encUser = utility.encrypt(req.body.username, req.session.hashtoken);

                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                    isLoggedIn: true,
                    isFirstTime: data.isFirstTime,
                    username: data.username,
                    isPaper: data.isPaper,
                    linkSelectSubject: config.redirectUri + "brand/" + encUser,
                    linkCheat: "linkCheat",
                    linkResultReport: "linkResultReport"

                });
            }
            else {
                logger.error("Entered password is incorrect", req.body.username);
                res.json({
                    errorCode: error.code_00001,
                    errorDesc: error.desc_00001,
                    isLoggedIn: false
                });
            }
        }
        else {
            logger.error("Entered username not found", req.body.username);
            res.json({
                errorCode: error.code_00001,
                errorDesc: error.desc_00001,
                isLoggedIn: false
            });
        }
    });
};

exports.logout = function (req, res) {
    req.session.destroy();
    res.json({
        errorCode: error.code_00000,
        errorDesc: error.desc_00000,
        isLoggedIn: false
    });
};

exports.verifyUser = function (req, res, next) {
    var decUser = decrypt(req.body.username);
    User.findOneAndUpdate({ username: decUser }, { isActive: true }, function (err, user) {
        if (err) {
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000
            });
        }
    });
};

exports.resetPassword = function (req, res, next) {
    var decUser = decrypt(req.body.username);
    var salt = crypto.randomBytes(16).toString('base64');
    var password = crypto.pbkdf2Sync(req.body.password, salt.toString(), 100, 64,'sha1').toString('base64');

    User.findOneAndUpdate({ username: decUser }, { password: password, salt: salt }, function (err, user) {
        if (err) {
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000
            });
        }
    });
};

var encrypt = function (text) {
    var cipher = crypto.createCipher(algorithm, config.Secret)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

var decrypt = function (text) {
    var decipher = crypto.createDecipher(algorithm, config.Secret)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

exports.userDetailByUsername = function (req, res, next, usernameDetail) {
    logger.info("Getting user detail", usernameDetail);
    User.aggregate(
        [
            {
                $match: { username: usernameDetail }
            },
            {
                $lookup:
                {
                    from: "testresults",
                    localField: "username",
                    foreignField: "username",
                    as: "score"
                }
            }
        ]
    ).exec(function (err, data) {
        req.user = data[0];
        if (data[0].score.length > 0 && !!data[0].score[0].testFrom && data[0].score[0].testFrom == "paper")
            req.user.testFrom = "paper";
        else if (data[0].score.length > 0 && !!data[0].score[0].testFrom && data[0].score[0].testFrom == "paper_delayed")
            req.user.testFrom = "paper_delayed";
        else req.user.testFrom = "online";
        
        delete req.user.score;
        next();
    })
};