var Slider = require('mongoose').model('slider');
var Event = require('mongoose').model('Event');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');
var fs = require('fs');
var algorithm = 'aes-256-ctr';
var base64 = require('base64-stream');
var stream = require('stream');

exports.createSlider = function (req, res, next) {
    var dataBuffer = new Buffer('');
    var base64data = "";
    var dataSave = {};
    req.pipe(req.busboy);

    req.busboy.on('field', function(fieldName, fieldValue, valTruncated,keyTruncated) {
        //so here I want to pause my stream 
        req.pause();

        dataSave = JSON.parse(fieldValue);

        req.resume();
    });

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log('File: ' + filename + ', mimetype: ' + mimetype);
        
        file.on('data', function (data) {
           dataBuffer = Buffer.concat([dataBuffer, data]);
        }).on('end', function () {
            base64data = dataBuffer.toString('base64');
            console.log("encrypted pdf to base64");
        });

        //Path where image will be uploaded
        // fstream = fs.createWriteStream(__dirname + '/img/' + filename);
        // file.pipe(fstream);
        // fstream.on('close', function () {
        //     console.log("Upload Finished of " + filename);
        // });
    }).on('finish', function () {

        dataSave.filePath = base64data;

        console.log("data Upload: ", dataSave);

        var slider = new Slider(dataSave);

        slider.save(function (err) {
            if (err) {
                logger.debug("Create Test Error: " + err.code + ", " + err.errmsg);
                if (!!err.code && !!err.errmsg) {
                  res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg
                  });
                } else {
                  res.json({
                    errorCode: error.code_00013,
                    errorDesc: error.desc_00013
                  });
                }
            } else {
                logger.debug("Create Test success");

                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
    });
};

exports.listSlider = function (req, res, next) {
    logger.debug("call List User");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType){ sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if( req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if( req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if(!!req.query.search){
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function(element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in Slider.schema.paths) {
            if (Slider.schema.paths.hasOwnProperty(property) && Slider.schema.paths[property]["instance"] === "String") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    console.log("queryArray: ", queryArray)

    console.log("query: ",query);
    Slider.find(query, { _id: 0, create: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, sliders) {
        if (err) {
            logger.debug("List Slider Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List Slider success");
            var returnData = {};
            returnData.data = sliders;

            Slider.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if(!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.readSlider = function (req, res) {
    if (!!req.slider) {
        res.json(req.slider);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.updateSlider = function (req, res, next) {
    if (!!req.slider) {
            Slider.findOneAndUpdate({ id: req.slider.id }, req.body, function (err, slider) {
                if (err) {
                    //return next(err);
                    res.json({
                        errorCode: err.code,
                        errorDesc: err.errmsg,
                    });
                } else {
                    res.json({
                        errorCode: error.code_00000,
                        errorDesc: error.desc_00000,
                    });
                }
            });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.updateSliderUpload = function (req, res, next) {
    var dataBuffer = new Buffer('');
    var base64data = "";
    var dataSave = {};
    req.pipe(req.busboy);

    req.busboy.on('field', function(fieldName, fieldValue, valTruncated,keyTruncated) {
        //so here I want to pause my stream 
        req.pause();

        dataSave = JSON.parse(fieldValue);

        req.resume();
    });

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log('File: ' + filename + ', mimetype: ' + mimetype);
        
        file.on('data', function (data) {
           dataBuffer = Buffer.concat([dataBuffer, data]);
        }).on('end', function () {
            base64data = dataBuffer.toString('base64');
            console.log("encrypted pdf to base64");
        });

        //Path where image will be uploaded
        // fstream = fs.createWriteStream(__dirname + '/img/' + filename);
        // file.pipe(fstream);
        // fstream.on('close', function () {
        //     console.log("Upload Finished of " + filename);
        // });
    }).on('finish', function () {

        Slider.findOneAndUpdate({ id: dataSave.id }, {filePath : base64data}, function (err, slider) {
                if (err) {
                    //return next(err);
                    res.json({
                        errorCode: err.code,
                        errorDesc: err.errmsg,
                    });
                } else {
                    res.json({
                        errorCode: error.code_00000,
                        errorDesc: error.desc_00000,
                    });
                }
            });
    });
};

exports.deleteSlider = function (req, res, next) {
    if (!!req.slider) {
        req.slider.remove(function (err) {
            if (err) {
                return next(err);
            } else {
                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.sliderByid = function (req, res, next, id) {
    logger.debug(id + "call sliderByid");
    Slider.findOne({
        id: id
    }, { create: 0, __v: 0 }, function (err, slider) {
        if (err) {
            logger.debug(id + "sliderByid Error");
            return next(err);
        } else {
            req.slider = slider;
            logger.debug(id + "sliderByid Success");
            next();
        }
    });
};

exports.sliderPreview = function(req, res, next) {
  Slider.find({}, { create: 0, __v: 0 }).sort({seqNumber:1}).exec(function(err, slider) {
    if (err) {
      res.json({
        errorCode: error.code_01003,
        errorDesc: error.desc_01003
      });
    } else {
      res.json({
        errorCode: error.code_00000,
        errorDesc: error.desc_00000,
        data: slider
      });
    }
  });
};

exports.createEvent = function (req, res, next) {
    logger.debug("call Create Event");

    req.body.startMonth = (new Date(req.body.startDate).getMonth()+1) + "" + (new Date(req.body.startDate).getFullYear());
    req.body.endMonth = (new Date(req.body.endDate).getMonth()+1) + "" + (new Date(req.body.endDate).getFullYear());
    req.body.description = req.body.desc;

    var event = new Event(req.body);

    event.save(function (err) {
        if (err) {
            logger.debug("Create Event Error: " + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("Create Event success");

            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
            });
        }
    });
};

exports.listEvent = function (req, res, next) {
    logger.debug("call List Event");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType){ sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if( req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if( req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if(!!req.query.search){
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function(element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in Event.schema.paths) {
            if (Event.schema.paths.hasOwnProperty(property) && Event.schema.paths[property]["instance"] === "String") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    console.log("queryArray: ", queryArray)

    console.log("query: ",query);
    Event.find(query, { _id: 0, salt: 0, password: 0, create: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, events) {
        if (err) {
            logger.debug("List Event Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List Event success");
            var returnData = {};
            returnData.data =events;

            Event.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if(!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.monthEvent = function (req, res, next) {
    Event.find({
        $or: [
            { startMonth: req.body.monthYear },
            { endMonth: req.body.monthYear }
        ]
    }, { _id: 0, create: 0, __v: 0 }, function (err, events) {
        if (err) {
            logger.debug(req.body.month + "get Event Month Error");
            return next(err);
        } else {
            var data = [];
            logger.debug(req.body.month + "get Event Month Success");
            console.log("events: ", events)
            events.forEach(function(e) {
                var event = {};
                event.desc = e.description;
                var stDate = new Date( e.startDate );
                var edDate = new Date( e.endDate );
                event.date = stDate.getDate() + "/" + (stDate.getMonth()+1) + " - " + edDate.getDate() + "/" + (edDate.getMonth()+1);
                data.push(event);
            }, this);
            
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
                data: data
            });
        }
    });
};

exports.eventCalendar = function (req, res, next) {
    Event.find({}, { _id: 0, create: 0, __v: 0 }, function (err, events) {
        if (err) {
            logger.debug(req.body.month + "get Event Calendar Error");
            return next(err);
        } else {
            var data = [];
            logger.debug(req.body.month + "get Event Calendar Success");
            events.forEach(function(e) {
                var event = {};
                event.title = e.title;
                if(!!e.url)
                    event.url = e.url
                else
                    event.url = "";
                event.allDay = true;
                
                var stDate = new Date( e.startDate );
                var edDate = new Date( e.endDate );
                var timeDiff = Math.abs(edDate.getTime() - stDate.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                var temp = [];
                for (var i = 0; i <= diffDays; i++) {
                    var event = {};
                    event.title = e.title;
                    if (!!e.url)
                        event.url = e.url
                    else
                        event.url = "";
                    event.allDay = true;
                    stDate.setDate(stDate.getDate() + 1);
                    event.start = new Date(stDate);
                    
                    data.push(event);
                }
                
            }, this);
            
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
                data: data
            });
        }
    });
};

exports.readEvent = function (req, res) {
    if (!!req.event) {
        res.json(req.event);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.updateEvent = function (req, res, next) {
    if (!!req.event) {

        req.body.startMonth = (new Date(req.body.startDate).getMonth()+1) + "" + (new Date(req.body.startDate).getFullYear());
        req.body.endMonth = (new Date(req.body.endDate).getMonth()+1) + "" + (new Date(req.body.endDate).getFullYear());
            Event.findOneAndUpdate({eventId: req.event.eventId }, req.body, function (err, event) {
                if (err) {
                    //return next(err);
                    res.json({
                        errorCode: err.code,
                        errorDesc: err.errmsg,
                    });
                } else {
                    res.json({
                        errorCode: error.code_00000,
                        errorDesc: error.desc_00000,
                    });
                }
            });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.deleteEvent = function (req, res, next) {
    if (!!req.event) {
        req.event.remove(function (err) {
            if (err) {
                return next(err);
            } else {
                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.eventByid = function (req, res, next, eventId) {
    logger.debug(eventId + "call eventByid");
    Event.findOne({
        eventId: eventId
    }, { create: 0, __v: 0 }, function (err, event) {
        if (err) {
            logger.debug(eventId + "eventByid Error");
            return next(err);
        } else {
            req.event = event;
            logger.debug(eventId + "eventByid Success");
            next();
        }
    });
};