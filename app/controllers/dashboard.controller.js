var CommentCheat = require('mongoose').model('CommentCheat');
var Contact = require('mongoose').model('Contact');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');

exports.listCommentCheat = function (req, res, next) {
    logger.info("Listing all CommentCheat");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = -1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType) { sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if (req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if (req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if (!!req.query.search) {
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function (element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in CommentCheat.schema.paths) {
            if (CommentCheat.schema.paths.hasOwnProperty(property) && CommentCheat.schema.paths[property]["instance"] === "String") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    CommentCheat.find(query, { _id: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, cc) {
        if (err) {
            logger.error("There was an error while trying to list all CommentCheat [" + err.code + ": " + err.errmsg + "]");
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.info("All CommentCheat listed successfully");
            var returnData = {};
            var dataTemp = JSON.stringify(cc);
            cc = JSON.parse(dataTemp);
            returnData.data = cc;
            returnData.data.forEach(function(e) {
                e.create = new Date( e.create );
                // console.log(timeTemp.getDate());
                // e.create = timeTemp.getDate()+"/"+(timeTemp.getMonth())+"/"+timeTemp.getFullYear()+", "+timeTemp.getHours()+":"+timeTemp.getMinutes()+":"+timeTemp.getSeconds()
            }, this);


            CommentCheat.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if (!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.listContact = function (req, res, next) {
    logger.info("Listing all Contact");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = -1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType) { sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if (req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if (req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if (!!req.query.search) {
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function (element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in Contact.schema.paths) {
            if (Contact.schema.paths.hasOwnProperty(property) && Contact.schema.paths[property]["instance"] === "String") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    Contact.find(query, { _id: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, contacts) {
        if (err) {
            logger.error("There was an error while trying to list all Contact [" + err.code + ": " + err.errmsg + "]");
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.info("All Contact listed successfully");
            var returnData = {};
            
            var dataTemp = JSON.stringify(contacts);
            contactscc = JSON.parse(dataTemp);
            returnData.data = contacts;
            returnData.data.forEach(function(e) {
                e.create = new Date( e.create );
                // console.log(timeTemp.getDate());
                // e.create = timeTemp.getDate()+"/"+timeTemp.getMonth()+"/"+timeTemp.getFullYear()+", "+timeTemp.getHours()+":"+timeTemp.getMinutes()+":"+timeTemp.getSeconds()
            }, this);

            Contact.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if (!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.read = function (req, res) {
    if (!!req.user) {
        res.json(req.user);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.delete = function (req, res, next) {
    if (!!req.user) {
        req.user.remove(function (err) {
            if (err) {
                return next(err);
            } else {
                res.json(req.user);
            }
        });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};