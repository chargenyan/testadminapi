var tokenUser = require('mongoose').model('tokenUser');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');
var jwt = require('jsonwebtoken');

exports.getToken = function (req, res, next) {
    tokenUser.findOne({
        username: req.body.username
    }, { _id: 0 }, function (err, user) {

        if (err) throw err;

        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {

            // check if password matches
            if (user.password != req.body.password) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
            } else {

                // if user is found and password is right
                // create a token
                console.log("Hello: ", user);

                var token = jwt.sign({username: user.username}, config.Secret, {
                    expiresIn: "6h" // expires in 6 hours
                });
                console.log("Hello2: ", token)
                // return the information including token as JSON
                res.json({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token
                });
            }

        }

    });
};
