var User = require('mongoose').model('User');
var TestResult = require('mongoose').model('TestResult');
var testFile = require('mongoose').model('testFile');

// var AnswerSheet = require('mongoose').model('AnswerSheet');
// var sumRanking = require('mongoose').model('sumRanking');
// var UserTest = require('mongoose').model('UserTest');
// var schoolDB = require('mongoose').model('school');

var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');
var crypto = require('crypto');
var utility = require('../../config/utility');

exports.reportBySubject = function (req, res, next) {
    const subject = req.body.testId;
    TestResult.where(subject).ne(null).lean().exec(function (err, docs) {
        var result = [];
        if (err) throw err;
        docs.forEach(function(doc) {
            User.findOne({username: doc.username}, function (err, user) {
                if (err) throw err;
                var data = {Username: doc.username, FirstName: "N/A", LastName: "N/A", SchoolCode: "N/A", SchoolName: "N/A", Score: doc[subject]};
                if (typeof user !== 'undefined' && user !== null)
                    data = { Username: doc.username, FirstName: user.firstName, LastName: user.lastName, SchoolCode: user.schoolCode, SchoolName: user.schoolName, Score: doc[subject]};
                result.push(data);
                if(result.length  === docs.length) {
                    res.xls('data.xlsx', result);
                }
            });
        });
    });
};

exports.reportAll = function (req, res, next) {
    const isBrand = req.body.isBrand;
    testFile.find({isBrand: isBrand}, { _id: 0, filePath: 0, ansTest: 0, create: 0, __v: 0 }).lean().exec(function (err, subjects) {
        if (err) {
            throw err;
        } else {
            console.log(subjects);
            var index = 0;
            TestResult.find({}, { _id: 0, results: 0, testFrom: 0, create: 0, __v: 0 }).lean().exec(function (err, docs) {
                var result = [];
                if (err) throw err;
                docs.forEach(function(doc) {
                    User.findOne({username: doc.username}, function (err, user) {
                        if (err) throw err;
                        var data = { Username: doc.username, FirstName: "N/A", LastName: "N/A", SchoolCode: "N/A", SchoolName: "N/A" };
                        if (typeof user !== 'undefined' && user !== null)
                            data = { Username: doc.username, FirstName: user.firstName, LastName: user.lastName, SchoolCode: user.schoolCode, SchoolName: user.schoolName };
                        var scoreList = {};
                        var numOfTest = 0;
                        subjects.forEach(function(subject){
                            scoreList[subject.testId] = doc[subject.testId];
                            if(doc[subject.testId] != null) numOfTest++;
                        });
                        if(numOfTest > 0)
                            result.push(Object.assign(data, scoreList));
                        if(++index === docs.length) {
                            res.xls('data.xlsx', result);
                        }
                    });
                });
            });
        }
    });
};