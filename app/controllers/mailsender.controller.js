'use strict';
const nodemailer = require('nodemailer');
var logger = require('../../config/log');
var error = require('../../config/error');
var config = require('../../config/config');
var User = require('mongoose').model('User');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var crypto = require('crypto');
var utility = require('../../config/utility');
var algorithm = 'aes-256-ctr';


exports.sendDirect = function (req, res, next) {

    var template = '<p><b> เรียน คุณ ' + req.body.name + ', </b></p>' +
        '<p>&nbsp;&nbsp;&nbsp;' + req.body.description + '</p>' +
        '<p>ขอแสดงความนับถือ,<p>' +
        '<p>ทีมงานบัณฑิตแนะแนว<p>';

    console.log("template:", template);

    let mailOptions = {
        from: '"Online Test BNN Support" <support@onlinetestbnn.com>',
        to: req.body.email,
        subject: 'Support',
        html: template
    };
    sendEmail(mailOptions, res);
};


exports.sendNotify = function (req, res, next) {
    var queryData = {};
    queryData.score = { $ne: [] };

    let transporter = nodemailer.createTransport({
        host: '127.0.0.1',
        port: 25,
        secure: false,
        ignoreTLS: true,
        // auth: {
        //     user: "support@xyphoze.net",
        //     pass: "70VchjjWtT8g"
        // }
    });

    User.aggregate([
        {
            $lookup:
            {
                from: "testresults",
                localField: "username",
                foreignField: "username",
                as: "score"
            }
        },
        {
            $match: queryData
        }
    ]).exec(function (err, data) {
        data.forEach(function (d) {

            var template = '<p><b> เรียน คุณ ' + data.username + ', </b></p>' +
                '<p>&nbsp;&nbsp;&nbsp;' + req.body.description + '</p>' +
                '<p>ขอแสดงความนับถือ,<p>' +
                '<p>ทีมงานบัณฑิตแนะแนว<p>';

            let mailOptions = {
                from: '"Online Test BNN Support" <support@onlinetestbnn.com>',
                to: d.email,
                subject: 'Support',
                html: template
            };

            transporter.sendMail(mailOptions, (err, info) => {
                if (err) {
                    res.json({
                        errorCode: error.code_04001,
                        errorDesc: error.desc_04001 + ": " + err,
                    });
                }
                console.log('Message sent: %s', info.messageId);

            });

        }, this);
        res.json({
            errorCode: error.code_00000,
            errorDesc: error.desc_00000,
        });
    })
};

var readTextFile = function (file) {
    logger.debug("file: " + file);
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    var allText
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status == 0) {
                allText = rawFile.responseText;
                // console.log(allText);
            }
        }
    }
    rawFile.send(null);
    return allText;
}

var sendEmail = function (mailOptions, res, dataPass) {
    let transporter = nodemailer.createTransport({
        host: '127.0.0.1',
        port: 25,
        secure: false,
        ignoreTLS: true,
        // auth: {
        //     user: "support@xyphoze.net",
        //     pass: "70VchjjWtT8g"
        // }
    });

    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            res.json({
                errorCode: error.code_04001,
                errorDesc: error.desc_04001 + ": " + err,
            });
        }
        else {
            console.log('Message sent: %s', info.messageId);
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
                data: dataPass
            });
        }
    });
};

var encrypt = function (text) {
    var cipher = crypto.createCipher(algorithm, config.Secret)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}