var Package = require('mongoose').model('Package');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');

exports.createPackage = function (req, res, next) {
    logger.debug("call Create Package");

    var package = new Package(req.body);

    package.save(function (err) {
        if (err) {
            logger.debug(package.id + "Create Package Error: " + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug(package.id + "Create Package success");

            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
            });
        }
    });
};

exports.listPackage = function (req, res, next) {
    logger.debug("call List User");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType){ sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if( req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if( req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if(!!req.query.search){
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function(element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in Package.schema.paths) {
            if (Package.schema.paths.hasOwnProperty(property) && Package.schema.paths[property]["instance"] === "String") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    console.log("queryArray: ", queryArray)

    console.log("query: ",query);
    Package.find(query, { _id: 0, create: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, packages) {
        if (err) {
            logger.debug("List Package Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List Package success");
            var returnData = {};
            returnData.data = packages;

            Package.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if(!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.readPackage = function (req, res) {
    if (!!req.package) {
        res.json(req.package);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.updatePackage = function (req, res, next) {
    if (!!req.package) {
            Package.findOneAndUpdate({ groupSubject: req.package.groupSubject }, req.body, function (err, package) {
                if (err) {
                    //return next(err);
                    res.json({
                        errorCode: err.code,
                        errorDesc: err.errmsg,
                    });
                } else {
                    res.json({
                        errorCode: error.code_00000,
                        errorDesc: error.desc_00000,
                    });
                }
            });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.deletePackage = function (req, res, next) {
    if (!!req.package) {
        req.package.remove(function (err) {
            if (err) {
                return next(err);
            } else {
                res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg,
                });
            }
        });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.packageByGroupSubject = function (req, res, next, GroupSubject) {
    logger.debug(GroupSubject + "call packageByGroupSubject");
    Package.findOne({
        groupSubject: GroupSubject
    }, { create: 0, __v: 0 }, function (err, package) {
        if (err) {
            logger.debug(GroupSubject + "packageByGroupSubject Error");
            return next(err);
        } else {
            req.package = package;
            logger.debug(GroupSubject + "packageByGroupSubject Success");
            next();
        }
    });
};
