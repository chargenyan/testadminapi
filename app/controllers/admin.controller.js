var Admin = require('mongoose').model('Admin');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');
var crypto = require('crypto');
var algorithm = 'aes-256-ctr';

exports.create = function (req, res, next) {
    logger.debug("call Create User");

    var permission = ["Home", "User", "TestFile", "Answersheet", "Report", "Payment"];
    req.body.permission = "";

    for (var i = 0; i < permission.length; i++) {
        req.body.permission += req.body[permission[i]] ? "1":"0";
        delete req.body[permission[i]];
    }

    var user = new Admin(req.body);

    if (req.body.email != "") {
        req.checkBody('email', 'Invalid email').isEmail();
        req.sanitizeBody('email').normalizeEmail();
        var errors = req.validationErrors();
        if (errors) {
            logger.debug(user.username + "Incorrect Email format: " + error.code_00010);
            res.json({
                errorCode: error.code_00010,
                errorDesc: error.desc_00010,
            });
            return;
        }
    }


    user.save(function (err) {
        if (err) {
            logger.debug(user.username + "Create User Error: " + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug(user.username + "Create User success");

            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
            });
        }
    });
};

exports.list = function (req, res, next) {
    logger.debug("call List User");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType){ sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if( req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if( req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if(!!req.query.search){
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function(element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in Admin.schema.paths) {
            if (Admin.schema.paths.hasOwnProperty(property) && Admin.schema.paths[property]["instance"] === "String" && property != "salt" && property != "password") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    console.log("queryArray: ", queryArray)

    console.log("query: ",query);
    Admin.find(query, { _id: 0, salt: 0, password: 0, create: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, admins) {
        if (err) {
            logger.debug("List Admin Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List Admin success");
            var returnData = {};
            var permission = ["Home", "User", "TestFile", "Answersheet", "Report", "Payment"];
            var dataTemp = JSON.stringify(admins);
            admins = JSON.parse(dataTemp);
            returnData.data = admins;
            for (var index = 0; index < admins.length; index++) {
                for (var i = 0; i < admins[index].permission.length; i++) {
                    admins[index][permission[i]] = admins[index].permission[i] == "1" ? true : false;
                }
                //SuperAdmin, Admin, Viewer
                if(admins[index].role == "SuperAdmin")
                    admins[index].configAdmin = true;
                else
                    admins[index].configAdmin = false;
                if(admins[index].role == "Viewer") 
                    admins[index].readonly = true;
                else
                    admins[index].readonly = false;
            }
            Admin.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if(!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.read = function (req, res) {
    if (!!req.user) {
        //SuperAdmin, Admin, Viewer
        if (req.user.role == "SuperAdmin")
            req.user.configAdmin = true;
        else
            req.user.configAdmin = false;
        if (req.user.role == "Viewer")
            req.user.readonly = true;
        else
            req.user.readonly = false;
        res.json(req.user);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.update = function (req, res, next) {
    if (!!req.user) {
        var isOldPassCorrect = true;
        if (!!req.body.password) {
            if (!!req.body.oldPassword) {
                var hashpwd = crypto.pbkdf2Sync(req.body.oldPassword, req.user.salt, 100, 64,'sha1').toString('base64');

                if (hashpwd == req.user.password) {
                    req.body.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
                    req.body.password = crypto.pbkdf2Sync(req.body.password, req.body.salt.toString(), 100, 64).toString('base64');
                }
                else
                    isOldPassCorrect = false;
            }
        }
        var permission = ["Home", "User", "TestFile", "Answersheet", "Report", "Payment"];
        req.body.permission = "";

        for (var i = 0; i < permission.length; i++) {
            req.body.permission += req.body[permission[i]] ? "1" : "0";
            delete req.body[permission[i]];
        }


        if (isOldPassCorrect) {
            Admin.findOneAndUpdate({ username: req.user.username }, req.body, function (err, user) {
                if (err) {
                    //return next(err);
                    res.json({
                        errorCode: err.code,
                        errorDesc: err.errmsg,
                    });
                } else {
                    if (!!user.username) {
                        var data = {};
                        data.username = user.username;
                        if(!!req.body.username)
                            data.username = req.body.username;
                    }
                    res.json({
                        errorCode: error.code_00000,
                        errorDesc: error.desc_00000,
                    });
                }
            });
        }
        else {
            res.json({
                errorCode: error.code_00011,
                errorDesc: error.desc_00011,
            });
        }
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.delete = function (req, res, next) {
    if (!!req.user) {
        req.user.remove(function (err) {
            if (err) {
                return next(err);
            } else {
                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003,
        });
    }
};

exports.adminByUsername = function (req, res, next, username) {
    logger.debug(username + "call adminByUsername");
    Admin.findOne({
        username: username
    }, { create: 0, __v: 0 }, function (err, admin) {
        if (err) {
            logger.debug(username + "adminByUsername Error");
            return next(err);
        } else {
            var permission = ["Home", "User", "TestFile", "Answersheet", "Report", "Payment"];
            for (var i = 0; i < admin.permission.length; i++) {
                admin[permission[i]] = admin.permission[i] == "1" ? true : false;
            }
            
            req.user = admin;
            logger.debug(username + "adminByUsername Success");
            next();
        }
    });
};

exports.checkListPermissionAdmin = function (req, res, next) {
    logger.debug(req.body.username + "call checkListPermissionAdmin");
    Admin.findOne({
        username: req.body.username
    }, { _id: 0, create: 0, __v: 0 }, function (err, admin) {
        if (err) {
            logger.debug(req.body.username + "checkListPermissionAdmin Error");
            return next(err);
        } else {
            var permission = ["Home", "User", "TestFile", "Answersheet", "Report", "Payment"];
            var ret = {};
            for (var i = 0; i < permission.length; i++) {
                if (admin.permission[i] == "1")
                    ret[admin.permission[i]] = true;
                else
                    ret[admin.permission[i]] = false;
            }
            if (admin.role == "SuperAdmin")
                ret["Admin"] = true;
            else
                ret["Admin"] = false;
            
            logger.debug(req.body.username + "checkListPermissionAdmin Success");
            res.json(ret);
        }
    });
};

exports.checkPermissionAdmin = function (req, res, next) {
    logger.debug(req.body.username + "call checkPermissionAdmin");
    Admin.findOne({
        username: req.body.username
    }, { _id: 0, create: 0, __v: 0 }, function (err, admin) {
        if (err) {
            logger.debug(req.body.username + "checkPermissionAdmin Error");
            return next(err);
        } else {
            var permission = ["Home", "User", "TestFile", "Answersheet", "Report", "Payment"];
            var ret = false;
            for (var i = 0; i < permission.length; i++) {
                if (permission[i] == req.body.page) {
                    if(admin.permission[i] == "1")
                        ret = true;
                    break;
                }
            }
            if (req.body.page == "Admin") {
                if (admin.role == "SuperAdmin")
                    ret = true;
                else
                    ret = false;
            }
            
            logger.debug(req.body.username + "checkPermissionAdmin Success");
            res.json(ret);
        }
    });
};

exports.login = function (req, res) {
    logger.debug(req.body.username + "call login User");
    req.checkBody({
        username: {
            notEmpty: true
        },
        password: {
            notEmpty: true
        }
    });
    logger.debug("validate Username and Password");
    var errors = req.validationErrors();
    if (errors) {
        logger.debug("Login Error: " + error.desc_00001);
        res.json({
            errorCode: error.code_00001,
            errorDesc: error.desc_00001,
            isLoggedIn: false
        });
        return;
    }
    logger.debug(req.body.username + "Start Query");
    var query = Admin.where( { username: req.body.username } );
    query.findOne(function (err, data) {
        if (err) {
            logger.debug(req.body.username + "Login Error: " + err.code + ", " + err.errmsg);
            res.json({
                errorCode: error.code_00001,
                errorDesc: error.desc_00001,
                isLoggedIn: false
            });
        }
        if (!!data && data.isActive) {
            logger.debug( req.body.username + "Start hash password to compare");
            var hashpwd = crypto.pbkdf2Sync(req.body.password, data.salt, 100, 64,'sha1').toString('base64');

            if (hashpwd == data.password) {
                logger.debug(req.body.username + "Login Success");
                if (req.body.remember === 'remember') {
                    req.session.remember = true;
                    req.session.username = req.body.username;
                    req.session.cookie.maxAge = 60000;
                }

                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                    isLoggedIn: true,
                    username: data.username,
                    role: data.role
                });
            }
            else {
                logger.debug(req.body.username + "Login Error Wrong Password: " + error.code_00001 + ", " + error.desc_00001);
                res.json({
                    errorCode: error.code_00001,
                    errorDesc: error.desc_00001,
                    isLoggedIn: false
                });
            }
        }
        else {
            logger.debug(req.body.username + "Login Error No username: " + error.code_00001 + ", " + error.desc_00001);
            res.json({
                errorCode: error.code_00001,
                errorDesc: error.desc_00001,
                isLoggedIn: false
            });
        }
    });
};

exports.logout = function (req, res) {
    req.session = null;
    res.json({
        errorCode: error.code_00000,
        errorDesc: error.desc_00000,
        isLoggedIn: false
    });
};

var decrypt = function (text) {
    var decipher = crypto.createDecipher(algorithm, config.Secret)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}
