var testFile = require('mongoose').model('testFile');
var cheatFile = require('mongoose').model('cheatFile');
var unzip = require('unzip-stream');
var base64 = require('base64-stream');
var stream = require('stream');
var error = require('../../config/error');
var logger = require('../../config/log');
var config = require('../../config/config');
var fs = require('fs');
var algorithm = 'aes-256-ctr';

exports.createTest = function (req, res, next) {
    var dataBuffer = new Buffer('');
    var base64data = "";
    var dataSave = {};
    req.pipe(req.busboy);

    req.busboy.on('field', function(fieldName, fieldValue, valTruncated,keyTruncated) {
        //so here I want to pause my stream 
        req.pause();

        dataSave = JSON.parse(fieldValue);

        req.resume();
    });

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log('File: ' + filename + ', mimetype: ' + mimetype);
        
        file.on('data', function (data) {
           dataBuffer = Buffer.concat([dataBuffer, data]);
        }).on('end', function () {
            base64data = dataBuffer.toString('base64');
            console.log("encrypted pdf to base64");
        });

        //Path where image will be uploaded
        // fstream = fs.createWriteStream(__dirname + '/img/' + filename);
        // file.pipe(fstream);
        // fstream.on('close', function () {
        //     console.log("Upload Finished of " + filename);
        // });
    }).on('finish', function () {

        dataSave.filePath = base64data;
        dataSave.testId = dataSave.testGroup + "_" + new Date().getTime();

        console.log("data Upload: ", dataSave);

        var test = new testFile(dataSave);

        test.save(function (err) {
            if (err) {
                logger.debug("Create Test Error: " + err.code + ", " + err.errmsg);
                res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg,
                });
            } else {
                logger.debug("Create Test success");

                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
    });
};

exports.createCheat = function (req, res, next) {
    var dataField = {};
    req.pipe(req.busboy);
    req.busboy.on('field', function(fieldName, fieldValue, valTruncated,keyTruncated) {
        //so here I want to pause my stream 
        req.pause();

        dataField = JSON.parse(fieldValue);

        req.resume();
    });
    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log('File: ' + filename + ', mimetype: ' + mimetype);

        file.pipe(unzip.Parse())
            .pipe(stream.Transform({
                objectMode: true,
                transform: function (entry, e, cb) {
                    if (entry.path.indexOf("/") == -1) {
                        var base64data = "";
                        var dataBuffer = new Buffer('');
                        entry.on('data', function (data) {
                                dataBuffer = Buffer.concat([dataBuffer, data]);
                            }).on('end', function () {

                                base64data = dataBuffer.toString('base64');

                                var data = {};
                                data.studyClass = 0;
                                data.testName = dataField.testId;
                                data.filePath = base64data;
                                data.no = entry.path.substring(0, entry.path.indexOf("."));
                                data.cheatNo = entry.path.substring(0, entry.path.indexOf("."));

                                var cheat = new cheatFile(data);
                                cheat.save(function (err) {
                                    if (err) {
                                        logger.debug("Create Test Error: " + err.code + ", " + err.errmsg);
                                    } else {
                                        logger.debug("Cheat Test success");
                                    }
                                });
                                cb();
                            });
                    }
                    else {
                        entry.autodrain();
                        cb();
                    }
                }
            }));

        //Path where image will be uploaded
        // fstream = fs.createWriteStream(__dirname + '/img/' + filename);
        // file.pipe(fstream);
        // fstream.on('close', function () {
        //     console.log("Upload Finished of " + filename);
        // });
    }).on('finish', function () {
        res.json({
            errorCode: error.code_00000,
            errorDesc: error.desc_00000,
        });
    });
};

exports.updateTest = function (req, res, next) {
    var base64data = "";
    var dataBuffer = new Buffer('');
    var dataSave = {};
    req.pipe(req.busboy);

    req.busboy.on('field', function(fieldName, fieldValue, valTruncated,keyTruncated) {
        //so here I want to pause my stream 
        req.pause();

        dataSave = JSON.parse(fieldValue);

        req.resume();
    });

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log('File: ' + filename + ', mimetype: ' + mimetype);
        
        file.on('data', function (data) {
           dataBuffer = Buffer.concat([dataBuffer, data]);
        }).on('end', function () {
            base64data = dataBuffer.toString('base64');
            console.log("encrypted pdf to base64");
        });

        //Path where image will be uploaded
        // fstream = fs.createWriteStream(__dirname + '/img/' + filename);
        // file.pipe(fstream);
        // fstream.on('close', function () {
        //     console.log("Upload Finished of " + filename);
        // });
    }).on('finish', function () {

        console.log("data Upload: ", dataSave);

        testFile.findOneAndUpdate({ testId: dataSave.testId }, { filePath: base64data }, function (err, testFile) {
            if (err) {
                //return next(err);
                res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg,
                });
            } else {
                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
    });
};

exports.publishTestSubject = function (req, res, next) {
    //{$or: [ {email: req.body.email}, {username: req.body.username} ]}
    var query = {};
    var queryArray = [];
    for (var i = 0; i < req.body.testId.length; i++) {
        queryArray.push({testId: req.body.testId[i]})
    }
    query["$or"] = queryArray;

    testFile.update(query, { $set:{isPublished: true} },{ multi: true }).exec(function (err, subject) {
        if (err) {
            logger.debug("List testFile Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List testFile success");
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
            });
        }
    });
};

exports.unpublishTestSubject = function (req, res, next) {
    var query = {};
    var queryArray = [];
    for (var i = 0; i < req.body.testId.length; i++) {
        queryArray.push({testId: req.body.testId[i]})
    }
    query["$or"] = queryArray;
    testFile.update(query, { $set:{isPublished: false} },{ multi: true }).exec(function (err, subject) {
        if (err) {
            logger.debug("List testFile Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List testFile success");
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
            });
        }
    });
};

exports.testSubject = function (req, res, next) {
    logger.info("Listing all testFile");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['firstName'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType) { sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if (req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if (req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if (!!req.query.search) {
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function (element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in testFile.schema.paths) {
            if (testFile.schema.paths.hasOwnProperty(property) && testFile.schema.paths[property]["instance"] === "String" && property != "filePath" && property != "ansTest") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }
    if(!!req.query.pagesize){
        testFile.find(query, { _id: 0, filePath: 0, ansTest: 0, __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, subjects) {
            if (err) {
                logger.error("There was an error while trying to list all testFile [" + err.code + ": " + err.errmsg + "]");
                res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg,
                });
            } else {
                logger.info("All testFile listed successfully");
                var returnData = {};
                returnData.data = subjects;

                testFile.aggregate(
                    [
                        {
                            $match: query
                        },
                        {
                            $count: "total"
                        }
                    ]
                ).exec(function (err, data) {
                    if (!!data && data.length > 0)
                        returnData.total = data[0].total;
                    else
                        returnData.total = 0;
                    res.json(returnData);
                })
            }
        });
    }
    else{
        testFile.find({}, { _id: 0, filePath: 0, ansTest: 0, create: 0, __v: 0 }).exec(function (err, subjects) {
            if (err) {
                logger.debug("List testFile Error :" + err.code + ", " + err.errmsg);
                res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg,
                });
            } else {
                var returnData = {};
                returnData.data = subjects;

                testFile.aggregate(
                    [
                        {
                            $match: query
                        },
                        {
                            $count: "total"
                        }
                    ]
                ).exec(function (err, data) {
                    if (!!data && data.length > 0)
                        returnData.total = data[0].total;
                    else
                        returnData.total = 0;
                    res.json(returnData);
                })
            }
        });
    }
};

exports.listCheat = function (req, res, next) {
    logger.info("Listing all cheatFile");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['firstName'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType) { sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if (req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if (req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }
    if (!!req.query.search) {
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function (element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }
    if (!!req.query.searchall) {
        for (var property in cheatFile.schema.paths) {
            if (cheatFile.schema.paths.hasOwnProperty(property) && cheatFile.schema.paths[property]["instance"] === "String" && property != "filePath" && property != "studyClass" && property != "no") {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    cheatFile.aggregate([
        { "$match": query},
        {
            "$group": {
                "_id": "$testName",
                "total": { "$sum": 1 }
            }
        },
        { "$sort": sort },
        { "$limit": pagesize * page },
        { "$skip": pagesize * (page - 1) }
    ]).exec(function (err, subjects) {
        if (err) {
            logger.error("There was an error while trying to list all cheatFile [" + err.code + ": " + err.errmsg + "]");
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.info("All cheatFile listed successfully");
            var returnData = {};
            returnData.data = subjects;

            cheatFile.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        "$group": {
                            "_id": "$testName"
                        }
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if (!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.testSubjectbyGroup = function (req, res, next) {
    testFile.find({ testGroup: req.body.testGroup}, { _id: 0, filePath: 0, ansTest: 0, create: 0, __v: 0 }).exec(function (err, subjects) {
        if (err) {
            logger.debug("List testFile Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List testFile success");
            res.json(subjects);
        }
    });
};

exports.SubjectGroup = function (req, res, next) {
    testFile.find().distinct("testGroup").exec(function (err, testGroup) {
        if (err) {
            logger.debug("List SubjectGroup Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List SubjectGroup success");
            res.json(testGroup);
        }
    });
};
