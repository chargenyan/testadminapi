var testFile = require('mongoose').model('testFile');
var cheatFile = require('mongoose').model('cheatFile');
var error = require('../../config/error');
var logger = require('../../config/log');
var utility = require('../../config/utility');

exports.read = function (req, res) {
    if(!!req.testFile){
        req.testFile.filePath = "data:application/pdf;base64," + req.testFile.filePath
        res.json(req.testFile);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003
        });
    }
};

exports.update = function (req, res, next) {
    testFile.findOneAndUpdate({ testId: req.testFile.testId }, req.body, function (err, testFile) {
        if (err) {
            //return next(err);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
            });
        }
    });
};

exports.delete = function (req, res, next) {
    req.testFile.remove(function (err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.testFile);
        }
    });
};

exports.testBytestId = function (req, res, next, testId) {
    testFile.findOne({
        testId: testId
    }, { create: 0, __v: 0 }, function (err, testFile) {
        if (err) {
            logger.error("get testFile failed");
            return next(err);
        } else {
            req.testFile = testFile;
            logger.info("get testFile successfully");
            next();
        }
    });
};

exports.getCheatId = function (req, res, next, cheatId) {
    req.cheatId = cheatId;
    next();
};

exports.deleteCheatFile = function (req, res, next) {
    cheatFile.remove({
        testName: req.cheatId
    }, function (err, cheatFile) {
        if (err) {
            logger.error("get testFile failed");
            res.json({
                errorCode: error.code_01003,
                errorDesc: error.desc_01003
            });
        } else {
            logger.info("get testFile successfully");
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
            });
        }
    });
};

exports.testNoAns = function (req, res, next) {
    testFile.find({ ansTest: "" }, { create: 0, __v: 0, filePath: 0 }, function (err, testFile) {
        if (err) {
            logger.error("get testNoAns failed");
            res.json({
                errorCode: error.code_01003,
                errorDesc: error.desc_01003
            });
        } else {

            logger.info("get testNoAns successfully");
            res.json({
                errorCode: error.code_00000,
                errorDesc: error.desc_00000,
                data: testFile
            });
        }
    });
};

exports.testWithAns = function (req, res, next) {
    logger.debug("call List User");
    var pagesize = 20;
    var page = 1;
    var sort = {}; sort['create'] = 1;
    var query = {};
    var queryArray = [];
    if (!!req.query.pagesize) pagesize = parseInt(req.query.pagesize);
    if (!!req.query.page) page = parseInt(req.query.page);
    if (!!req.query.sortType){ sort = {}; sort[req.query.sortType] = 1; }
    if (!!req.query.sortBy) {
        if( req.query.sortBy == "ASC")
            sort[req.query.sortType] = 1;
        else if( req.query.sortBy == "DESC")
            sort[req.query.sortType] = -1;
    }

    query["ansTest"] = /./;

    if(!!req.query.search){
        var tmpeQ = req.query.search.split(';');
        tmpeQ.forEach(function(element) {
            if (element.split(':')[1] == "true") {
                query[element.split(':')[0]] = true;
            }
            else if (element.split(':')[1] == "false") {
                query[element.split(':')[0]] = false;
            }
            else {
                query[element.split(':')[0]] = element.split(':')[1];
            }
        }, this);
    }

    if (!!req.query.searchall) {
        for (var property in testFile.schema.paths) {
            if (testFile.schema.paths.hasOwnProperty(property) && testFile.schema.paths[property]["instance"] === "String" && property != "filePath" ) {
                var addObject = {};
                addObject[property] = new RegExp(req.query.searchall, "i");
                queryArray.push(addObject);
            }
        }
        query["$or"] = queryArray;
    }

    console.log("queryArray: ", queryArray)

    console.log("query: ",query);

    testFile.find(query, { filePath: 0 , __v: 0 }).sort(sort).skip(pagesize * (page - 1)).limit(pagesize).exec(function (err, tests) {
        if (err) {
            logger.debug("List Admin Error :" + err.code + ", " + err.errmsg);
            res.json({
                errorCode: err.code,
                errorDesc: err.errmsg,
            });
        } else {
            logger.debug("List Admin success");
            var returnData = {};

            var dataTemp = JSON.stringify(tests);
            tests = JSON.parse(dataTemp);
            returnData.data = tests;

            testFile.aggregate(
                [
                    {
                        $match: query
                    },
                    {
                        $count: "total"
                    }
                ]
            ).exec(function (err, data) {
                if(!!data && data.length > 0)
                    returnData.total = data[0].total;
                else
                    returnData.total = 0;
                res.json(returnData);
            })
        }
    });
};

exports.ansTestUpdate = function (req, res, next) {
    testFile.findOneAndUpdate({ testId: req.body.testId }, { ansTest: req.body.ansTest }, function (err, testFile) {
            if (err) {
                //return next(err);
                res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg,
                });
            } else {
                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
};

exports.ansTestClear = function (req, res, next) {
    testFile.findOneAndUpdate({ testId: req.body.testId }, { ansTest: "" }, function (err, testFile) {
            if (err) {
                //return next(err);
                res.json({
                    errorCode: err.code,
                    errorDesc: err.errmsg,
                });
            } else {
                res.json({
                    errorCode: error.code_00000,
                    errorDesc: error.desc_00000,
                });
            }
        });
};

exports.ansTestBytestId = function (req, res, next, testId2) {
    testFile.findOne({
        testId: testId2
    }, { create: 0, __v: 0, filePath: 0 }, function (err, testFile) {
        if (err) {
            logger.error("get testFile failed");
            return next(err);
        } else {
            req.testFile = testFile;
            logger.info("get testFile successfully");
            next();
        }
    });
};

exports.readAnsTest = function (req, res) {
    if(!!req.testFile){
        res.json(req.testFile);
    }
    else {
        res.json({
            errorCode: error.code_01003,
            errorDesc: error.desc_01003
        });
    }
};