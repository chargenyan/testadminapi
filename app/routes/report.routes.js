module.exports = function(app) {
    var report = require('../controllers/report.controller');
    app.post('/reportBySubject', report.reportBySubject);
    app.post('/reportAll', report.reportAll);
};