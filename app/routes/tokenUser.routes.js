module.exports = function(app) {
    var tokenUser = require('../controllers/tokenUser.controller');
    var admin = require('../controllers/admin.controller');
    app.post('/getToken', tokenUser.getToken);
    app.post('/loginAdmin', admin.login);
    app.post('/logoutAdmin', admin.logout);
};