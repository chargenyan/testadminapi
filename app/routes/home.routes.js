module.exports = function(app) {
    var home = require('../controllers/home.controller');
    app.route('/slider')
        .post(home.createSlider)
        .get(home.listSlider);
    app.route('/slider/:id')
        .get(home.readSlider)
        .put(home.updateSlider)
        .delete(home.deleteSlider);

    app.route('/sliderPreview').get(home.sliderPreview);
    app.route('/updateSliderUpload').post(home.updateSliderUpload);

    app.route('/event')
        .post(home.createEvent)
        .get(home.listEvent);
    app.route('/event/:eventId')
        .get(home.readEvent)
        .put(home.updateEvent)
        .delete(home.deleteEvent);
    app.route('/eventCalendar').post(home.eventCalendar);
    app.route('/monthEvent').post(home.monthEvent);

    app.param('id', home.sliderByid);
    app.param('eventId', home.eventByid);

};