module.exports = function(app) {
    var package = require('../controllers/package.controller');
    app.route('/package')
        .post(package.createPackage)
        .get(package.listPackage);
    app.route('/package/:GroupSubject')
        .get(package.readPackage)
        .put(package.updatePackage)
        .delete(package.deletePackage);

    app.param('GroupSubject', package.packageByGroupSubject);

};