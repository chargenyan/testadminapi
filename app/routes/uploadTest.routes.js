module.exports = function (app) {
    var uploadTest = require('../controllers/uploadTest.controller');
    app.route('/uploadtest')
        .post(uploadTest.createTest)
    app.route('/uploadcheat')
        .post(uploadTest.createCheat)
    app.route('/testSubject')
        .get(uploadTest.testSubject)
    app.route('/listCheat')
        .get(uploadTest.listCheat)
    app.route('/testSubjectbyGroup')
        .get(uploadTest.testSubjectbyGroup)
    app.route('/publishTestSubject')
        .post(uploadTest.publishTestSubject)
    app.route('/unpublishTestSubject')
        .post(uploadTest.unpublishTestSubject)
    app.route('/SubjectGroup')
        .get(uploadTest.SubjectGroup);
    app.route('/updateTestFile')
        .post(uploadTest.updateTest)

        
};