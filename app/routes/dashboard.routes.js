module.exports = function(app) {
    var dashboard = require('../controllers/dashboard.controller');
    app.route('/contact')
        .get(dashboard.listContact);
    app.route('/comment')
        .get(dashboard.listCommentCheat);

};