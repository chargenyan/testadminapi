module.exports = function(app) {
    var mailsender = require('../controllers/mailsender.controller');
    app.post('/sendDirect', mailsender.sendDirect);
    app.post('/sendNotify',mailsender.sendNotify);
};