module.exports = function(app) {
    var admin = require('../controllers/admin.controller');
    app.route('/admin')
        .post(admin.create)
        .get(admin.list);
    app.route('/admin/:admin')
        .get(admin.read)
        .put(admin.update)
        .delete(admin.delete);
    app.route('/checkListPermissionAdmin')
        .post(admin.checkListPermissionAdmin)
    app.route('/checkPermissionAdmin')
        .post(admin.checkPermissionAdmin)

    app.param('admin', admin.adminByUsername);

};