module.exports = function(app) {
    var user = require('../controllers/user.controller');
    app.post('/login', user.login);
    app.post('/logout', user.logout);
    app.route('/user')
        .post(user.create)
        .get(user.list);
    app.route('/user/:username')
        .get(user.read)
        .put(user.update)
        .delete(user.delete);
    app.route('/chkuser')
        .post(user.checkUsername);
    app.route('/checkIdCard')
        .post(user.checkIdCard);
    app.post('/verifyUser',user.verifyUser);
    app.post('/resetPassword',user.resetPassword);
    app.route('/userDetail/:usernameDetail')
        .get(user.read)
    app.route('/register').post(user.register)
    app.route('/register/:username')
        .get(user.read)
        .put(user.updateReg)
        .delete(user.delete);
        
    app.route('/userBrand/:usernameBrand')
        .get(user.read)


    app.param('username', user.userByUsername);
    app.param('usernameBrand', user.userByUsernameBrand);
    app.param('usernameDetail', user.userDetailByUsername);

};