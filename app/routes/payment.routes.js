module.exports = function(app) {
    var payment = require('../controllers/payment.controller');
    app.route('/payment')
        .get(payment.listPayment);
    app.route('/paymentDetail')
        .post(payment.paymentDetail);
    app.route('/paymentReject')
        .post(payment.paymentReject);
    app.route('/paymentApprove')
        .post(payment.paymentApprove);
    

};