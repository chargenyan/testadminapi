module.exports = function(app) {
    var testFile = require('../controllers/testFile.controller');
    app.route('/testFile/:testId')
        .get(testFile.read)
        .put(testFile.update)
        .delete(testFile.delete);
    app.route('/deleteCheatFile/:cheatId')
        .delete(testFile.deleteCheatFile)
    
    app.route('/testNoAns').get(testFile.testNoAns)
    app.route('/testWithAns').get(testFile.testWithAns)
    app.route('/ansTestUpdate').post(testFile.ansTestUpdate)
    app.route('/ansTestClear').post(testFile.ansTestClear)
    app.route('/ansTest/:testId2')
        .get(testFile.readAnsTest)
    
    
    

    app.param('cheatId', testFile.getCheatId)
    app.param('testId', testFile.testBytestId);
    app.param('testId2', testFile.ansTestBytestId);
};