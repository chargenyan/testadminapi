var mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
var Schema = mongoose.Schema;

var sliderSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    filePath: {
        type: String,
        required: true
    },
    seqNumber: {
        type: String,
        required: true,
        unique: true,
    },
    link: {
        type: String,
        default: ""
    },
    create: {
        type: Date,
        default: Date.now
    }
}, { collection: 'slider' });
sliderSchema.plugin(AutoIncrement, {inc_field: 'id'});

mongoose.model('slider', sliderSchema);