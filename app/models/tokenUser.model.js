var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tokenUserSchema = new Schema({
    username: {
        type: String, 
        unique: true, 
        trim: true,
        required: 'User is required'
    },
    password: String
},  { collection: 'tokenUser' });

mongoose.model('tokenUser', tokenUserSchema);