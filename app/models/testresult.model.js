var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TestResultSchema = new Schema({
    username: {
        type: String, 
        unique: true, 
        trim: true,
        required: 'User is required'
    },
    create: {
        type: Date,
        default: Date.now
    }
},{ strict: false },  { collection: 'TestResult' });

mongoose.model('TestResult', TestResultSchema);