var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schoolSchema = new Schema({
    SCHOOL_ID: String,
    SCHOOL_NAME: String,
    PROVINCE_NAME: String,
    DISTRICT_NAME: String

},  { collection: 'school' });

mongoose.model('school', schoolSchema);