var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentCheatSchema = new Schema({
    firstName: String,
    lastName: String,
    email: {
        type: String,
        index: true,
        trim: true,
        match: /.+\@.+\.+/
    },
    phone: String,
    testName: String,
    noSubject: Number,
    testId: String,
    description: String,
    status: String,
    create: {
        type: Date,
        default: Date.now
    }
},  { collection: 'CommentCheat' });

mongoose.model('CommentCheat', CommentCheatSchema);