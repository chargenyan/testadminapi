var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TestFileSchema = new Schema({
    testId: {
        type: String,
        unique: true,
        trim: true,
        required: true,
        index: true
    },
    testName: {
        type: String,
        required: true,
        index: true
    },
    filePath: {
        type: String,
        required: true
    },
    ansTest: {
        type: String,
        default: ""
    },
    itemNo: {
        type: Number,
        required: true
    },
    itemStart: {
        type: String,
        default: "1"
    },
    choiceType: {
        type: Number,
        required: true
    },
    initTime: {
        type: Number,
        required: true,
        index: true
    },
    testType: {
        type: String,
        required: true,
        index: true
    },
    testGroup: {
        type: String,
        required: true,
        index: true
    },
    checkAns: {
        type: String,
        default: ""
    },
    maxScore: {
        type: Number,
        required: true,
        index: true
    },
    isBrand: {
        type: Boolean,
        default: false
    },
    isPublished: {
        type: Boolean,
        default: false,
        index: true
    },
    create: {
        type: Date,
        default: Date.now,
        index: true
    }
}, { collection: 'testFile' });
mongoose.model('testFile', TestFileSchema);