var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CheatFileSchema = new Schema({
    testName: {
        type: String,
        required: true,
        index: true
    },
    studyClass: {
        type: String,
        required: true,
        index: true
    },
    no: {
        type: String,
        required: true,
        index: true
    },
    cheatNo: {
        type: Number,
        required: true,
        index: true
    },
    filePath: {
        type: String,
        required: true
    },
    create: {
        type: Date,
        default: Date.now,
        index: true
    }
}, { collection: 'cheatFile' });

mongoose.model('cheatFile', CheatFileSchema);