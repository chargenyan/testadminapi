var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sumRankingSchema = new Schema({
    username: {
        type: String, 
        unique: true, 
        trim: true,
        required: 'User is required'
    },
    totalScore: Number,
    rankingType: String,
    ranking: Number

},  { collection: 'sumRanking' });

mongoose.model('sumRanking', sumRankingSchema);