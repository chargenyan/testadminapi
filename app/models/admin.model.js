var mongoose = require('mongoose');
var crypto = require('crypto');
var Schema = mongoose.Schema;
var logger = require('../../config/log');

var AdminSchema = new Schema({
    username: {
        type: String,
        unique: true,
        trim: true,
        required: 'User is required'
    },
    email: {
        type: String,
        index: true,
        trim: true,
        match: /.+\@.+\.+/
    },
    password: {
        type: String,
        // validate: [
        //     function (password) {
        //         return password && password.length >= 8;
        //     },
        //     'Password must be at least 8 characters'
        // ]
    },
    role: String,
    permission: String,
    isActive: {
        type: Boolean,
        default: true
    },
    salt: {
        type: String
    },
    create: {
        type: Date,
        default: Date.now
    }
}, { collection: 'admins' });

AdminSchema.pre('save', function (next) {
    if (this.password) {
        logger.debug("Create salt");
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
        logger.debug("hashed");
    }
    next();
});

AdminSchema.methods.hashPassword = function (password) {
    logger.debug("hashing password");
    return crypto.pbkdf2Sync(password, this.salt, 100, 64,'sha1').toString('base64');
};

AdminSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

mongoose.model('Admin', AdminSchema);
