var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var packageSchema = new Schema({
    groupSubject: {
        type: String, 
        required: 'groupSubject is required',
        unique: true
    },
    price: Number,
    eachPrice: Number,
    create: {
        type: Date,
        default: Date.now
    }
}, { collection: 'Package' });

mongoose.model('Package', packageSchema);

//TESTFINAL
//เป็นเด็กดีนะ