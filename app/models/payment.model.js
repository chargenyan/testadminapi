var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var paymentSchema = new Schema({
    username: {
        type: String, 
        trim: true,
        required: 'User is required'
    },
    price: Number,
    order: Array,
    status: String,
    fileSlip: String,
    create: {
        type: Date,
        default: Date.now
    }
}, { collection: 'Payment' });

mongoose.model('Payment', paymentSchema);