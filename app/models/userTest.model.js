var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserTestSchema = new Schema({
    username: {
        type: String, 
        unique: true, 
        trim: true,
        required: 'User is required'
    },
    test: {
        type: Array, 
        required: true
    },
    create: {
        type: Date,
        default: Date.now
    }
},{ strict: false },  { collection: 'userTests' });

mongoose.model('UserTest', UserTestSchema);