var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContactSchema = new Schema({
    firstName: String,
    lastName: String,
    email: {
        type: String,
        index: true,
        trim: true,
        match: /.+\@.+\.+/
    },
    topic: String,
    description: String,
    status: String,
    create: {
        type: Date,
        default: Date.now
    }
},  { collection: 'Contact' });

mongoose.model('Contact', ContactSchema);