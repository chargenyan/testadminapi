var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnswerSheetSchema = new Schema({
    username: {
        type: String, 
        unique: true, 
        trim: true,
        required: 'User is required'
    },
    create: {
        type: Date,
        default: Date.now
    }
},{ strict: false },  { collection: 'AnswerSheet' });

mongoose.model('AnswerSheet', AnswerSheetSchema);