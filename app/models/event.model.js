var mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
var Schema = mongoose.Schema;

var EventSchema = new Schema({
    startDate: Date,
    endDate: Date,
    title: String,
    description: String,
    startMonth: Number,
    endMonth: Number,
    url: String,
    create: {
        type: Date,
        default: Date.now
    }
},  { collection: 'Event' });
EventSchema.plugin(AutoIncrement, {inc_field: 'eventId'});

mongoose.model('Event', EventSchema);