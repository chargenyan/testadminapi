var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userRankingSchema = new Schema({
    username: {
        type: String, 
        unique: true, 
        trim: true,
        required: 'User is required'
    }
},  { collection: 'userRanking' });

mongoose.model('userRanking', userRankingSchema);