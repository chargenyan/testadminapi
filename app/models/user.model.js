var mongoose = require('mongoose');
var crypto = require('crypto');
var Schema = mongoose.Schema;
var logger = require('../../config/log');

var UserSchema = new Schema({
    firstName: String,
    lastName: String,
    username: {
        type: String,
        unique: true,
        trim: true,
        required: 'User is required'
    },
    nickName: String,
    birthDate: String,
    sex: String,
    idCard: {
        type: String
    },
    studyClass: String,
    avgGrade: String,
    schoolRegion: String,
    schoolProvince: String,
    schoolDistrict: String,
    schoolName: String,
    schoolCode: String,
    university: String,
    faculty: String,
    homeNo: String,
    soi: String,
    street: String,
    district: String,
    city: String,
    province: String,
    postcode: String,
    email: {
        type: String,
        index: true,
        trim: true,
        match: /.+\@.+\.+/
    },
    emailParent: {
        type: String,
        index: true,
        trim: true,
        match: /.+\@.+\.+/
    },
    password: {
        type: String,
        // validate: [
        //     function (password) {
        //         return password && password.length >= 8;
        //     },
        //     'Password must be at least 8 characters'
        // ]
    },
    mobilePhone1: String,
    mobilePhone2: String,
    lineId: String,
    lineIdParent: String,
    isActive: {
        type: Boolean,
        default: false
    },
    isFirstTime: {
        type: Boolean,
        default: false
    },
    isPaper: {
        type: Boolean,
        default: false
    },
    brandCode:{
        type:Array
    },

    salt: {
        type: String
    },
    create: {
        type: Date,
        default: Date.now
    }
}, { collection: 'users' });

UserSchema.pre('save', function (next) {
    if (this.password) {
        logger.debug("Create salt");
        this.salt = crypto.randomBytes(16).toString('base64');
        this.password = this.hashPassword(this.password);
        logger.debug("hashed");
    }
    next();
});

UserSchema.methods.hashPassword = function (password) {
    logger.debug("hashing password");
    return crypto.pbkdf2Sync(password, this.salt, 100, 64,'sha1').toString('base64');
};

UserSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

mongoose.model('User', UserSchema);
