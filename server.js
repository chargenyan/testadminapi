process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var mongoose = require('./config/mongoose');
var express = require('./config/express');
var passport = require('./config/passport');
const settings = require('./config/settings.json');
var bodyParser = require('body-parser');
var cluster = require('cluster');
var logger = require('./config/log');

var db = mongoose();
var app = express();
var passport = passport();

var port = process.env.PORT || 8252;

exports.conn = db.connection;

//app.listen(port);

if(cluster.isMaster) {
    var numWorkers = require('os').cpus().length;

    logger.debug('Master cluster setting up ' + (numWorkers) + ' workers...');
    logger.debug('Server running at '+ port);

    for(var i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
        logger.debug('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
        logger.debug('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        logger.debug('Starting a new worker');
        cluster.fork();
    });
} else {
    //app.all('/*', function(req, res) {res.send('process ' + process.pid + ' says hello!').end();})

    var server = app.listen(port, function() {
        logger.debug('Process ' + process.pid + ' is listening to all incoming requests');
    });
}


// parse application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

module.exports = app;
