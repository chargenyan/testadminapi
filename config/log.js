var mongo = require('mongoskin');
var config = require('./config');
var db = mongo.db(config.mongoUri);
var conf = {
    transport : function(data) {
        var logdb = db.collection("logs");
        logdb.insert({ 
            level: data.title,
            message: data.args[0],
            user: data.args[1],
            file: data.file,
            timestamp: data.timestamp,
        }, function(err, log) {
            if (err) console.error(err); 
        });
    }, 
    level: 'debug'
}
var logger = require('tracer').console(conf);

module.exports = logger