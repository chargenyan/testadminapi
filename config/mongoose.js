var config = require('./config');
var mongoose = require('mongoose');

module.exports = function() {
    mongoose.set('debug', config.debug);
    var db = mongoose.connect(config.mongoUri, { useMongoClient: true });

    require('../app/models/user.model');
    require('../app/models/post.model');
    require('../app/models/postdetail.model');
    require('../app/models/comment.model');
    require('../app/models/testresult.model');
    require('../app/models/answerSheet.model');
    require('../app/models/testFile.model');
    require('../app/models/userTest.model');
    require('../app/models/tokenUser.model');
    require('../app/models/contact.model');
    require('../app/models/cheatFile.model');
    require('../app/models/commentCheat.model');
    require('../app/models/admin.model');
    require('../app/models/school.model');
    require('../app/models/userRanking.model');
    require('../app/models/sumRanking.model');
    require('../app/models/slider.model');
    require('../app/models/event.model');
    require('../app/models/payment.model');
    require('../app/models/package.model');

    return db;
};