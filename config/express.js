var config = require('./config');
var express = require('express');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var sass = require('node-sass-middleware');
var validator = require('express-validator');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');
var cors = require('cors');
var jwt = require('jsonwebtoken');
var useragent = require('express-useragent');
var os = require('os');
var ifaces = os.networkInterfaces();
var utility = require('./utility');
var busboy = require('connect-busboy');
var json2xls = require('json2xls');

module.exports = function () {
    var app = express();
    var whitelist = ['https://onlinetestbnn.com', 'http://qa.onlinetestbnn.com', 'http://dev.onlinetestbnn.com', 'http://admin.dev.onlinetestbnn.com', 'http://localhost:3000', "http://localhost:4200"]
    const corsPolicy = {
        origin:  function (origin, callback) {
            if (whitelist.indexOf(origin) !== -1) {
              callback(null, true)
            } else {
              callback(new Error('Unauthorized access'))
            }
        },
        methods: 'GET,PUT,POST,DELETE,OPTIONS',
        allowedHeaders: 'Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept, X-Language, X-Session, x-userTokenId, x-accessId, Pragma, Cache-Control, If-Modified-Since, Cookie, x-access-token',
        credentials:true
    }
    app.use(cors(corsPolicy));

    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    } else {
        //app.use(compression);
    }
    app.set('trust proxy', 1)
    app.use(session({
        secret: [config.Secret],
        saveUninitialized: false,
        resave: false,
        store: new MongoStore({ 
            url: config.mongoUri,
            collection: 'sessions',
            ttl: 5 * 60 * 60,
            autoRemove: 'disabled'
        }),
        cookie: { 
            maxAge: 5 * 60 * 60 * 1000,
            httpOnly : true,
            secure: false
        }
    }));

    app.use(busboy());

    app.use(useragent.express());

    app.use(passport.initialize());
    //app.use(passport.session());

    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());
    app.use(validator());

    app.use(json2xls.middleware);
    
    app.set('views', './app/views');
    app.set('view engine', 'pug');

    require('../app/routes/tokenUser.routes')(app);

    app.use(function (req, res, next) {
        var username = req.session.username
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        if(!!req.body.isEnc){
            if(!!req.body.username){
                if (req.session.hashtoken !== null && typeof req.session.hashtoken !== "undefined" && req.session.hashtoken != "") {
                    req.body.username = utility.decryptSecret(req.body.username, req.session.hashtoken);
                    if (req.body.username != req.session.username)
                        return res.json({ errorCode: '00009', errorDesc: 'Different User' });
                }
                else{
                    res.json({ errorCode: '00002', errorDesc: 'Invalid token.' });
                }
            }
            if(!!req.body.Subject)
                req.body.Subject = utility.decryptSecret(req.body.Subject,req.body.username + req.session.hashtokenforSubject);
        }
        // decode token
        if (username) {
            // TODO: verify if user is valid
            next();
        }
        else if (token) {

            // verifies secret and checks exp
            jwt.verify(token, config.Secret, function (err, decoded) {
                if (err) {
                    return res.json({ errorCode: '00002', errorDesc: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        }
        else {
            // if there is no token
            // return an error
            return res.status(403).send({
                errorCode: '00006',
                errorDesc: 'No token provided.'
            });
        }
    });

    require('../app/routes/admin.routes')(app);
    require('../app/routes/dashboard.routes')(app);
    require('../app/routes/mailsender.routes')(app);
    require('../app/routes/home.routes')(app);
    require('../app/routes/user.routes')(app);
    require('../app/routes/report.routes')(app);
    require('../app/routes/uploadTest.routes')(app);
    require('../app/routes/payment.routes')(app);
    require('../app/routes/testFile.routes')(app);
    require('../app/routes/package.routes')(app);

    app.use(sass({
        src: './sass',
        dest: './public/css',
        outputStyle: 'compressed',
        prefix: '/css',
        debug: true
    }));

    app.use(express.static('./public'));

    return app;
}